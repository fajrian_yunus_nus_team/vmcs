This repository is created for the purpose of the second workshop (i.e. assignment) of the **Software Quality Management (a.k.a unit 7)** module of the **SE23PT5 team** from [NUS](http://www.nus.edu.sg/) [Institute of Systems Science](https://www.iss.nus.edu.sg/) [Master of Technology (Software Engingeering)](https://www.iss.nus.edu.sg/graduate-programmes/programme/detail/master-of-technology-in-software-engineering) programme in the **academic year 2016/2017, semester 1**.

### Team Members

| Name   |      Student ID      |  Bitbucket Username |  Role |
|----------|:-------------:|------:|------:|
| Cai Peng |  A0135875U | caipeng | Technical lead |
| Fajrian Yunus |    A0135876R   |   fajrian_yunus_nus | Analyst |
| Jesmond Lee Qiong Wei |    A0135878M   |   JesLee | User representative |
| Lim Wei Keong |    A0135899H   |   dr3am3r4ev3r | PM |
| Rexon Mulingtapang Guico |    A0120542X   |   rexguico | QA |
| Roshan Parjapat |    A0134611U   |   roshannus | Developer |
| Saw Thanda Oo |    A0135881Y   |   A0135881 | CM |
| Zhu Beibei |    A0135949N   |   beibeizhu | Developer |

